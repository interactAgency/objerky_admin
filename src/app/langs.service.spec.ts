/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { LangsService } from './langs.service';

describe('Service: Langs', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LangsService]
    });
  });

  it('should ...', inject([LangsService], (service: LangsService) => {
    expect(service).toBeTruthy();
  }));
});
