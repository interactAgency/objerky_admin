import { Component, OnInit } from '@angular/core';
import { OrdersService } from '../orders.service';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'orders-list',
  templateUrl: './orders-list.component.html',
  styleUrls: ['./orders-list.component.sass'],
  providers: [OrdersService]
})
export class OrdersListComponent implements OnInit {

  orders;
  details = false;
  
  constructor(
    private _os: OrdersService
  ) { }
  

  ngOnInit() {
    this._os.getOrders().toPromise()
    .then( res => {
      this.orders = res.json().Orders.reverse()
      console.log( res.json() );
    } )
  }

}
