import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { OrdersService } from '../orders.service';
import { ProductsService } from '../products.service';

@Component({
  selector: 'order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.sass'],
  providers: [OrdersService, ProductsService]
})
export class OrderDetailsComponent implements OnInit, OnChanges {

  @Input() order;

  constructor(
    private _os: OrdersService,
    private _ps: ProductsService
  ) { }
  
  ngOnInit(){
    // for( let i = 0; i < this.order.Products; i++ ){
    //   this._ps.getProduct( this.order.Products[i].ProductId, 'ru' )
    //     .subscribe( res => {
    //       console.log( res.json() );
    //     } )
    // }
    // console.log( this.order.Products );
  }

  ngOnChanges() {
    
  }

}
