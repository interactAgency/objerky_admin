import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class ProductsService {

  constructor(
    private _http: Http
  ) { }

  getProducts(){
    return this._http.get( "http://api.objerky.com/Products/All" )
  }

  getProduct( id, lang ){
    return this._http.get( "http://api.objerky.com/Products/"+id+"?lang="+lang )
  }

  saveProductDef( product ){
    return this._http.patch( "http://api.objerky.com/Products/"+product.Id, product )
  }

  saveProduct( product, lang ){
    return this._http.post( "http://api.objerky.com/Products/lang/"+product.Id+"?lang="+lang, product )
  }

  createProduct( product ){
    return this._http.post( "http://api.objerky.com/Products/Add", product )
  }

  deleteProduct( id ){
    return this._http.delete( "http://api.objerky.com/Products/"+id )
  }

  uploadImg(fileToUpload: any, id) {
    return this._http
        .post("http://objerky.com/api/Picture/Product/"+id, fileToUpload);
  }

}
