import { Component, OnInit } from '@angular/core';
import { LangsService } from '../langs.service';
import { FileUploadService } from '../file-upload.service';


@Component({
  selector: 'lang-editor',
  templateUrl: './lang-editor.component.html',
  styleUrls: ['./lang-editor.component.sass'],
  providers: [LangsService, FileUploadService]
})
export class LangEditorComponent implements OnInit {

  langs;
  errMess;

  constructor(
    private _ls: LangsService,
    private _fu: FileUploadService
  ) { }

  ngOnInit() {
    this.getLangs()
  }

  getLangs(){
    this._ls.getLangs().toPromise()
      .then( res => {
        this.langs = res.json().Languages;
        
      } )
  }

  addLang( fi, name, code ){
    if( name !== '' && code !== '' && fi.files && fi.files[0] ){
     this._ls.addLang( name, code ).toPromise()
      .then( res => {
        if( res.json().Status === "Failed" ){
          this.errMess = res.json().Reason;
        }else{
          this.errMess = '';
          console.log( res.json() );
          let fileToUpload = fi.files[0];
          this._fu
              .upload(fileToUpload, res.json().Id, 'Lang').toPromise()
              .then(res => {
                  location.reload();
              });
          
          //this.getLangs();
        }
      } )
    }else{
      this.errMess = 'Заполните все поля';
    }
  }
  
  rmLang( i, id ){
    this._ls.rmLang( id ).toPromise()
      .then( res => {
        this.langs.splice( i, 1 );
      } )
  }

}
