import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';


@Injectable()
export class AuthService {

  constructor(
    private _http: Http
  ) { }

  login( login, pass ){
    let body = {
      login: login,
      password: pass
    }
    return this._http.post( 'http://api.objerky.com/home/login', body);
  }

}
