import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
  providers: [UsersService]
})
export class UsersComponent implements OnInit {

  users;
  saleUsers = [];

  constructor(
    private _us: UsersService
  ) { }

  ngOnInit() {
    this.getUsers()
  }

  toSale( i, id, email ){
    this.saleUsers.push( { i: i, id: id, email: email } );
    this.users[i].saling = true;
  }

  getUsers(){
    this._us.getUsers().toPromise()
    .then( res => {
      this.users = res.json().Users;
      for( let i = 1; i < this.users.length; i++ ){
        this.users[i].editor = false;
        this.users[i].saling = false;
      }
    } )
  }



  rmSaleUser( e ){
    this.saleUsers.splice( e.saleI, 1 );
    this.users[e.i].saling = false;
  }

  saveSale( i, size, id ){
    let user = [ id ];
    this.users[i].dqLoad = true;
    this._us.refreshSale( size, user ).toPromise()
      .then( res => {
        this.users[i].dqLoad = false;
      } )
    this.users[i].editor = false;

    // TODO: UPDATING SIZE 
  }

  makeSale( s ){
    if( s === 'Success' ){
      this.getUsers();
    }
  }


}
