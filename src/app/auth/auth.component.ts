import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.sass'],
  providers: [AuthService]
})
export class AuthComponent implements OnInit {

  constructor(
    private _as: AuthService,
    private router: Router
  ) { }

  private errorMess;

  ngOnInit() {
  }

  // login( login, pass ){
  //   this._as.login( login, pass ).subscribe( res => {
  //     console.log(res.status);
  //     localStorage.setItem( 'adminLogin', login );
  //     this.router.navigate(['/products']);
  //   } )
  // }

  login( login, pass ){
    this._as.login( login, pass )
    .toPromise()
    .then( res => {
      localStorage.setItem( 'adminLogin', login );
      this.router.navigate(['/products']);
    } )
    .catch( () => {
      this.errorMess = 'Неверный логин или пароль';
    });
  }

}
