import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
 
@Injectable()
export class ProductsGuard implements CanActivate{
  public allowed: boolean;
 
  constructor(private router: Router) { }
 
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      let adminLogin = localStorage.getItem( 'adminLogin' );
      if( adminLogin === null ){
        this.router.navigate(['/']);
        return false;
      }else{
        return true;
      }
  }
}