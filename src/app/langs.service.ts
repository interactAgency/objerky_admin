import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class LangsService {

  constructor(
    private _http: Http
  ) { }

  getLangs(){
    return this._http.get( "http://api.objerky.com/Lang/All" )
  }

  addLang( name, code ){
    let data = {
      Code: code,
      Name: name
    }
    return this._http.post( "http://api.objerky.com/Lang", data )
  }

  rmLang( id ){
    return this._http.delete( "http://api.objerky.com/Lang/"+id )
  }

}
