import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule }   from '@angular/router';

import { AppComponent } from './app.component';
import { AuthComponent } from './auth/auth.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ProductsComponent } from './products/products.component';
import { AuthGuard } from './auth-guard';
import { ProductsGuard } from './products-guard';
import { ProductsListComponent } from './products-list/products-list.component';
import { ProductEditorComponent } from './product-editor/product-editor.component';
import { LangsSwitcherComponent } from './langs-switcher/langs-switcher.component';
import { ProductCreateComponent } from './product-create/product-create.component';
import { OrdersComponent } from './orders/orders.component';
import { OrdersListComponent } from './orders-list/orders-list.component';
import { OrderDetailsComponent } from './order-details/order-details.component';
import { SliderComponent } from './slider/slider.component';
import { UsersComponent } from './users/users.component';
import { SalesComponent } from './sales/sales.component';
import { PageComponent } from './page/page.component';
import { LangEditorComponent } from './lang-editor/lang-editor.component';
import { SettingsComponent } from './settings/settings.component';

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    NavbarComponent,
    ProductsComponent,
    ProductsListComponent,
    ProductEditorComponent,
    LangsSwitcherComponent,
    ProductCreateComponent,
    OrdersComponent,
    OrdersListComponent,
    OrderDetailsComponent,
    SliderComponent,
    UsersComponent,
    SalesComponent,
    PageComponent,
    LangEditorComponent,
    SettingsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([
      {
        path: '',
        component: AuthComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'products',
        component: ProductsComponent,
        canActivate: [ProductsGuard]
      },
      {
        path: 'newproduct',
        component: ProductCreateComponent,
        canActivate: [ProductsGuard]
      },
      {
        path: 'orders',
        component: OrdersComponent,
        canActivate: [ProductsGuard]
      },
      {
        path: 'slider',
        component: SliderComponent,
        canActivate: [ProductsGuard]
      },
      {
        path: 'users',
        component: UsersComponent,
        canActivate: [ProductsGuard]
      },
      {
        path: 'page/:page',
        component: PageComponent,
        canActivate: [ProductsGuard]
      },
      {
        path: 'settings',
        component: SettingsComponent,
        canActivate: [ProductsGuard]
      }
    ])
  ],
  providers: [AuthGuard, ProductsGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
