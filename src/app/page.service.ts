import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class PageService {

  constructor(
    private _http: Http
  ) { }

  getPageTrans( page ){
    return this._http.get( "http://api.objerky.com/Lang/Translations/"+page)
  }

  setPageTrans( id, trans ){
    let data = {};
    data[id] = trans;
    
    return this._http.post( "http://api.objerky.com/Lang/Translations", data)
  }

}
