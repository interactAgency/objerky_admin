import { Component, OnInit, OnChanges } from '@angular/core';
import { SliderService } from '../slider.service';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.sass'],
  providers: [SliderService]
})
export class SliderComponent implements OnChanges, OnInit {

  currSlide;
  slides;

  constructor(
    private _ss: SliderService
  ) { }

  getSlides(){
    this._ss.getSlides().toPromise()
      .then( res => {
        this.currSlide = res.json().Slider[0];
        this.slides = res.json().Slider;
        //console.log( res.json().Slider )
      } )
  }

  ngOnChanges() {
    this.getSlides()
  }
  ngOnInit() {
   this.getSlides()
  }

  getSlide(slide){
    this.currSlide = slide;
  }

  addFile(fi, pos): void {
    if (fi.files && fi.files[0]) {
        let fileToUpload = fi.files[0];
        this._ss
            .updateImg(fileToUpload, this.currSlide, pos).toPromise()
            .then(res => {
                location.reload();
                //console.log( res );
            });
    }
  }

  newSlide( front, back, island, arrow ){
    let args = arguments;
    if( front.img.files && front.img.files[0] ){
      let fileToUpload = front.img.files[0];
      this._ss
        .addSlide(fileToUpload).toPromise()
        .then(res => {
            for( let i = 1; i < args.length; i++ ){
              this._ss
                .updateImg(args[i].img.files[0], res.json().Id, args[i].name).toPromise()
                .then(res => {
                    if( i === args.length - 1 ){
                      location.reload();
                    }
                });
            }
        });
    }

  }

  rmSlide( i ){
    this._ss.rmSlide(this.currSlide).toPromise()
      .then( res => {
        this.slides.splice( i, 1 );
        console.log( res.json() );
        // this.getSlide( res.json().Prev );
        // this.currSlide = res.json().Prev;
      } )
    
  }

}
