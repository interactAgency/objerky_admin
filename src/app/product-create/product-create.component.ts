import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../products.service';
import { FileUploadService } from '../file-upload.service';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-product-create',
  templateUrl: './product-create.component.html',
  styleUrls: ['./product-create.component.css'],
  providers: [ProductsService, FileUploadService]
})
export class ProductCreateComponent implements OnInit {

  url = '';
  isMain = true;

  constructor(
    private _ps: ProductsService
  ) { }

  ngOnInit() {
  }

  createProduct( newImage, newBoxAmount, newBoxPrice, newItemPrice, newName, newDescription, isMain ){
    let newProduct = {
      isMain: isMain,
      BoxAmount: newBoxAmount,
      BoxPrice: newBoxPrice,
      ItemPrice: newItemPrice,
      Name: newName,
      Description: newDescription,
      Url: this.url
    }
    
    
    if (newImage.files && newImage.files[0]) {
        let fileToUpload = newImage.files[0];
        this._ps.createProduct( newProduct ).toPromise()
          .then( res => {
            this._ps
              .uploadImg(fileToUpload, res.json().Id).toPromise()
              .then(res => {
                  location.replace( location.origin+"/products" )
              });
          } )
    }
  }
  

}