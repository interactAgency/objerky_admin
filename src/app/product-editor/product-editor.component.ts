import { Component, OnChanges, Input } from '@angular/core';
import { ProductsService } from '../products.service';
import { FileUploadService } from '../file-upload.service';
import 'rxjs/add/operator/toPromise';


@Component({
  selector: 'product-editor',
  templateUrl: './product-editor.component.html',
  styleUrls: ['./product-editor.component.sass'],
  providers: [ProductsService, FileUploadService]
})
export class ProductEditorComponent implements OnChanges {

  @Input() product;
  currentLang = "ru";
  oldUrl;
  loadRus = false;
  loadAll = false;


  constructor(
    private _ps: ProductsService
  ) { }

  ngOnChanges() {
    
  }

  switchLang( event ){
    this.product = event.product;
    this.currentLang = event.lang;
    //console.log( this.product );
    //console.log(this.currentLang);
  }

  saveProduct( newBoxAmount, newBoxPrice, newItemPrice, newName, newDescription ){
    let defProduct = {
      Id: this.product.Id,
      IsMain: this.product.IsMain,
      Weight: this.product.Weight,
      BoxAmount: newBoxAmount,
      BoxPrice: newBoxPrice,
      ItemPrice: newItemPrice,
      Name: newName,
      Description: newDescription,
      Url: this.product.Url
    }
    let product = {
      Id: this.product.Id,
      Name: newName,
      Description: newDescription
    }
    if( this.currentLang === 'ru' ){
      this.loadRus = true;
      this._ps.saveProductDef( defProduct ).toPromise()
        .then( res => {
          console.log( res.json() );
          this.product = res.json().Product;
          this.loadRus = false;
        } )
    }else{
      this.loadAll = true;
      this._ps.saveProduct( product, this.currentLang ).toPromise()
        .then( res => {
          console.log( res.json() );
          this.product = res.json().Product;
          this.loadAll = false;
        } )
    }

  }

  addFile(fi): void {
    if (fi.files && fi.files[0]) {
        let fileToUpload = fi.files[0];
        this._ps
            .uploadImg(fileToUpload, this.product.Id)
            .subscribe(res => {
                location.reload();
            });
    }
  }

}
