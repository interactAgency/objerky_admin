import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { LangsService } from '../langs.service';
import { ProductsService } from '../products.service';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'langs-switcher',
  templateUrl: './langs-switcher.component.html',
  styleUrls: ['./langs-switcher.component.css'],
  providers: [LangsService, ProductsService]
})
export class LangsSwitcherComponent implements OnInit {

  @Input() productId;
  @Output() switchingLang = new EventEmitter();

  langs;
  toHide = 'ru';

  constructor(
    private _ls: LangsService,
    private _ps: ProductsService
  ) { }

  ngOnInit() {
    this._ls.getLangs().toPromise()
    .then( res => {
      this.langs = res.json().Languages;
      //console.log( res.json().Languages );
    } )
  }

  switchLang( productId, lang ){
    this.toHide = lang;
    this._ps.getProduct( productId, lang ).toPromise()
    .then( res => {
      let event = {
        product: res.json().Product,
        lang: lang
      }
      console.log( res.json() );
      
      this.switchingLang.emit( event )
      // console.log(res.json().Product);
    } )
  }

}
