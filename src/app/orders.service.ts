import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class OrdersService {

  constructor(
    private _http: Http
  ) { }

  getOrders(){
    return this._http.get( "http://api.objerky.com/Orders/ru" )
  }

}
