import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';

import { PageService }  from '../page.service';
import { LangsService } from '../langs.service';

@Component({
  selector: 'page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.sass'],
  providers: [PageService, LangsService]
})
export class PageComponent implements OnInit {

  pageData;
  langs;
  currLang;
  allLoad;

  
  constructor(
    private _route: ActivatedRoute,
    private _ls: LangsService,
    private _ps: PageService
  ) { }

  ngOnInit() {
    this.getTrans()
    this.getLangs()
    this.getCurrLang()
  }


  getTrans(){
    this.allLoad = true;
    this._route.params.forEach((params: Params) => {
      let page = params['page'];
      this._ps.getPageTrans( page ).toPromise()
        .then( res => {
          this.allLoad = false;
          this.pageData = res.json().Result;
        } )
    });
  }

  getLangs(){
    this._ls.getLangs().toPromise()
      .then( res => {
        this.langs = res.json().Languages
      } )
  }


  getCurrLang(){
    this._ls.getLangs().toPromise()
      .then( res => {
        let langs = res.json().Languages;
        for( let i = 0; i < langs.length; i++ ){
          if( langs[i].Code === 'ru' ){
            this.currLang = langs[i].Id;
          }
        }
      } )
  }



  changeLang( lang ){
    this.currLang = lang;
  }

  setPageTrans( id, trans ){
    this.allLoad = true;
    this._ps.setPageTrans( id, trans ).toPromise()
      .then( res => {
        this.allLoad = false;
        if( res.json().Status === "Success" ){
          this.getTrans()
        }
      } )
  }

}
