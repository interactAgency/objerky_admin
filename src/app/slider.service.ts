import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

@Injectable()
export class SliderService {

  constructor(
    private _http: Http
  ) { }

  getSlides(){
    return this._http.get( "http://api.objerky.com/Slider" )
  }

  updateImg( fileToUpload, id, pos? ){
    
    return this._http
      .post( "http://objerky.com/api/Picture/Slider/"+id+"/"+pos, fileToUpload);

    // var headers = new Headers();
    // headers.append('Content-type', 'multipart/form-data');
    // return this._http
    //   .post( "http://objerkyua-001-site1.dtempurl.com/api/Picture/Slider/"+id+"/"+pos, fileToUpload, { headers: headers } )   
  }

  addSlide( head ){
    return this._http.post( "http://objerky.com/api/Picture/Slider", head ) 
  }

  rmSlide( slide ){
    return this._http.delete( "http://objerky.com/api/Picture/Slider/"+slide ) 
  }

}
