import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class UsersService {

  constructor(
    private _http: Http
  ) { }

  getUsers(){
    return this._http.get( "http://api.objerky.com/Users" )
  }

  makeSale( size, saleUsers, from? ){
    let data = {
      discount: size,
      ids: []
    };
    for( let i = 0; i < saleUsers.length; i++ ){
      data.ids.push( saleUsers[i].id );
    }
    return this._http.post( "http://api.objerky.com/Users/Discount?from="+from, data )
  }

  refreshSale( size, user ){
    let data = {
      discount: size,
      ids: user
    }
    return this._http.post( "http://api.objerky.com/Users/Discount?from="+0, data )
  }

}
