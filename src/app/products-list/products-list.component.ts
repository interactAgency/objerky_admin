import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ProductsService } from '../products.service';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css'],
  providers: [ProductsService]
})
export class ProductsListComponent implements OnInit {

  @Output() editProduct = new EventEmitter();

  constructor(
    private _ps: ProductsService
  ) { }

  products;

  ngOnInit() {

    this._ps.getProducts().toPromise()
    .then( res => {
      this.products = res.json().Products;
      //console.log(res.json().Products);
    } )

  }

  onEditProduct( product ){
    this.editProduct.emit( product );
  }

  deleteProduct(i, id){
    this.products.splice( i, 1 );
    this._ps.deleteProduct( id ).toPromise()
    .then( res => {
      // console.log( res );
    } )
  }


}
