import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UsersService } from '../users.service';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.css'],
  providers: [UsersService]
})
export class SalesComponent implements OnInit {

  @Input() saleUsers;
  @Output() rmSaleUser = new EventEmitter();
  @Output() makingSale = new EventEmitter();

  constructor(
    private _us: UsersService
  ) { }

  ngOnInit() {
    
  }
  
  changeLog: string[] = [];

  rmUser( i, saleI ){
    let e = {i: i, saleI: saleI}
    this.rmSaleUser.emit( e );
  }

  makeSale( size, from? ){
    this._us.makeSale( size, this.saleUsers, from ).toPromise()
      .then( res => {
        if( res.json().Status === "Success" ){
          this.makingSale.emit( "Success" )
        }
      } )
  }


}
