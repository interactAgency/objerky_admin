import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

@Injectable()
export class FileUploadService {

  constructor(
    private http: Http
  ){}

  upload(fileToUpload: any, id, place) {
    let fd = new FormData();
    fd.append("image", fileToUpload);
    let headers = new Headers();
    headers.set('Content-Type', 'multipart/form-data');

    return this.http
        .post("http://objerky.com/api/Picture/"+place+"/"+id, fileToUpload, {
            headers: headers
        });
  }

}
