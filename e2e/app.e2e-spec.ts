import { ObjerkyAdminPage } from './app.po';

describe('objerky-admin App', function() {
  let page: ObjerkyAdminPage;

  beforeEach(() => {
    page = new ObjerkyAdminPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
